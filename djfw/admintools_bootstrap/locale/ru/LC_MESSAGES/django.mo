��    3      �  G   L      h     i  &        �     �     �     �     �     �            1   ,     ^     e     m     y     �     �     �     �     �     �     �     �     �     �     �     �  	   �               (  	   C  @   M  �   �               &     :     R     W     l     �     �  *   �     �  >   �       	        %     2  �  M     
  3   
      N
     o
     x
     �
  (   �
  "   �
     �
  #     G   /     w     �     �     �  )   �     �            
        )     2  
   H     S     `     x     �     �     �     �  *   �       }   &  �   �  
   b     m  &   |  6   �     �  6   �  G   $  $   l  
   �  5   �     �  F   �     1     F      R  *   s     
   #   (   *   "                ,                       /   0                -                         !       +                 %             1                  .   	          $                 3   '           &   2                           )         By %(filter_title)s  %(counter)s result %(counter)s results %(full_result_count)s total %(name)s Add Add %(name)s Add another %(verbose_name)s Change my password Change password Clear selection Click here to select the objects across all pages Delete Delete? Django site Documentation Enter username and password Filter Go History Home Log in Log in again Log out Modules New password Next Old password Open site Password (again) Password change Password change successful Password: Please correct the error below. Please correct the errors below. Please enter your old password, for security's sake, and then enter your new password twice so we can verify you typed it in correctly. Previous Remove Remove from sorting Run the selected action Save Save and add another Save and continue editing Save as new Search Select all %(total_count)s %(module_name)s Show all Thanks for spending some quality time with the Web site today. Toggle sorting Username: View on site Your password was changed. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-01-26 13:20+0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
  по %(filter_title)s  %(counter)s запись %(counter)s записей всего %(full_result_count)s %(name)s Добавить Добавить %(name)s Добавить еще %(verbose_name)s Сменить мой пароль Сменить пароль Сбросить выделение Нажмите тут, что бы выбрать все объекты Удалить Удалить? Джанго админка Документация Введите логин и пароль Фильтр Вперед История Домой Вход Зайти снова Выйти Модули Новый пароль Вперед Старый пароль Открыть сайт Пароль (снова) Сменить пароль Пароль успешно изменен Пароль: Пожалуйста, исправьте ошибку ниже Пожалуйста, исправьте ошибки ниже Пожалуйста, введите ваш старый пароль, а затем дважды новый пароль, что быпроверить правильность ввода. Назад Удалить Убрать из сортировки Выполнить выбранное действие Сохранить Сохранить и добавить еще один Сохранить и продолжить редактирование Сохранить как новый Поиск Выбрать все %(total_count)s %(module_name)s Показать все Спасибо за то, что уделили время сайту! Сортировка Логин: Смотреть на сайте Ваш пароль был изменен. 