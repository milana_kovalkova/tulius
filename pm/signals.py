from django.dispatch import Signal

private_message_created = Signal(providing_args=[])