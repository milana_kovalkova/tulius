from django.utils.translation import ugettext_lazy as _
from djfw.cataloging.core import CatalogPage

index_catalog_page = CatalogPage(name=_('Tulius'), url='/')